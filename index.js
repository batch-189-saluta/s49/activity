// console.log('hello')

/*
	fetch() - is a method in javascript, which allows to send request to an api and process its response. fetch has 2 arguements, the url to resource/route and optional object which contains additional information about our requests such as method, body, and headers of our request

	fetch method
		syntax:
			fetch(url, options)
*/


// Get post data using fetch()------------------------------------

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => console.log(data))


// add post data-----------------------------------------------------

document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {

		method: 'POST',
		headers: {'Content-Type': 'application/json; charset=UTF-8'},
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert("Successfully added")

		// to clear the input fields after posting
		document.querySelector('#txt-title').value = null
		document.querySelector('#txt-body').value = null
	})
})


// show post after request-------------------------------------------

const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;

}

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data))



// edit post------------------------------------------------------------

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body

	// removes the disabled attribute from the button
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
}


// update post after edit post--------------------------------------

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: 'PUT',
		headers: {'Content-type': 'application/json; charset=UTF-8'},
		body: JSON.stringify({

			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value
		})
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('Successfully updated')

		document.querySelector('#txt-edit-id').value=null
		document.querySelector('#txt-edit-title').value=null
		document.querySelector('#txt-edit-body').value=null

		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
})

// retrieve a single post-----------------------------------

fetch('https://jsonplaceholder.typicode.com/posts/5')
.then((response) => response.json())
.then((data) => console.log(data))



// deleting a post-----------------------------------------------------------

const deletePost = (id) => {
	console.log(id)

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: 'DELETE'
	})
	.then((response) => response.json())
	.then((data) => {
		document.querySelector(`#post-${id}`).remove()
	})
}